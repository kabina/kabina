import React from 'react'
import {render, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import { Kabina } from './Kabina';
import mockAxios from 'jest-mock-axios';


describe('Kabina4', () => {
    it('axios called2', () => {
      mockAxios.get.mockResolvedValueOnce([]);
      
      render(<Kabina />);
      //screen.getByText('Customer:');
      expect(screen.getByText('Request a bus:')).toBeInTheDocument();
   
    });
  });
