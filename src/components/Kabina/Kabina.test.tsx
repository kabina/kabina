import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { Kabina } from './Kabina';


describe('Kabina', () => {
    it('renders Kabina component', () => {
      render(<Kabina />);
      expect(screen.getByText('Request a bus:')).toBeInTheDocument();
    });
  });
  