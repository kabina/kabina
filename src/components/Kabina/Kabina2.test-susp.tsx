import React from 'react'
import {rest} from 'msw'
import {setupServer} from 'msw/node'
import {render, fireEvent, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import { Kabina, Order } from './Kabina';


const server = setupServer(
  rest.get('http://localhost:8080/orders', (req, res, ctx) => {
    return res(ctx.status(200),ctx.json([{"Id":2340662,"From":0,"To":1,"Wait":10,"Loss":30,"Distance":19,"Shared":true,"InPool":false,"Status":"RECEIVED","Received":"2023-09-02T19:51:41.506306+02:00","Started":null,"Completed":null,"AtTime":null,"Eta":-1,"Cab":{"Id":-1,"Location":-1,"Status":"CHARGING"},"CustId":1,"RouteId":-1,"LegId":-1},{"Id":2317617,"From":4211,"To":4212,"Wait":15,"Loss":70,"Distance":1,"Shared":true,"InPool":true,"Status":"COMPLETED","Received":"2023-08-03T11:14:31.097537+02:00","Started":"2023-08-03T11:28:53.112461+02:00","Completed":"2023-08-03T11:30:56.411580+02:00","AtTime":null,"Eta":0,"Cab":{"Id":3155,"Location":4429,"Status":"FREE"},"CustId":1,"RouteId":11,"LegId":-1}]))
  }),
  rest.get('http://localhost:8080/stops', (req, res, ctx) => {
    return res(ctx.status(200),ctx.json([{"id":0,"bearing":180,"latitude":47.507803,"longitude":19.235276,"name":"Erdei bekötőút"},{"id":1,"bearing":-180,"latitude":47.49069,"longitude":19.10891,"name":"Hidegkuti Nándor Stadion"},{"id":2,"bearing":-180,"latitude":47.500001,"longitude":19.023809,"name":"Déli pályaudvar M"}]))
  }),
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

describe('Kabina2', () => {
    it('axios called', () => {
      server.use(
        rest.get('http://localhost:8080/orders', (req, res, ctx) => {
          return res(ctx.status(200),ctx.json([{"Id":2340662,"From":0,"To":1,"Wait":10,"Loss":30,"Distance":19,"Shared":true,"InPool":false,"Status":"RECEIVED","Received":"2023-09-02T19:51:41.506306+02:00","Started":null,"Completed":null,"AtTime":null,"Eta":-1,"Cab":{"Id":-1,"Location":-1,"Status":"CHARGING"},"CustId":1,"RouteId":-1,"LegId":-1},{"Id":2317617,"From":4211,"To":4212,"Wait":15,"Loss":70,"Distance":1,"Shared":true,"InPool":true,"Status":"COMPLETED","Received":"2023-08-03T11:14:31.097537+02:00","Started":"2023-08-03T11:28:53.112461+02:00","Completed":"2023-08-03T11:30:56.411580+02:00","AtTime":null,"Eta":0,"Cab":{"Id":3155,"Location":4429,"Status":"FREE"},"CustId":1,"RouteId":11,"LegId":-1}]))
        })
      );
      render(<Kabina />);
      //screen.getByText('Customer:');
      expect(screen.getByText('Request a bus:')).toBeInTheDocument();
   
    });
  });
  