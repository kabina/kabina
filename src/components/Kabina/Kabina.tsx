/* eslint-disable no-restricted-globals */
import axios from "axios";
import React, { FunctionComponent, useEffect, useState, useReducer } from "react";
import logo from '../../kabina-logo.png';
import { Table } from "./Table";
import QRCode from "react-qr-code";
import './Kabina.css'

export const Kabina: FunctionComponent = () => {
    const host = 'http://localhost:8080'
    const [stops, setStops] = useState<Array<Stop>>([]);
    const [orders, setOrders] = useState<Array<TaxiOrder>>([]);
    const [inputs, setInputs] = useState<Input>({
        status : "",
        fromStand : 0, // TODO: has to be the first ID in stops, not just zero
        toStand : 0,
        maxWait : 10, // how long can I wait for a cab
        maxLoss : 30, // [%] how long can I lose while in pool
        shared : true,
        atTime : ""
    });
   //const [custId, setCustId] = useState<number>(0);
    const [isShown, setIsShown] = useState(false);
    const [, forceUpdate] = useReducer(x => x + 1, 0);
    
    /*if (location && location.search) {
        const queryParams = new URLSearchParams(location.search)
        const id = queryParams.get("cust_id")

        if (id) {
            setCustId(Number(id));
        }
    }
    */
    const columns = [
        { Header: 'Stop', accessor: 'stop'},
        { Header: 'Status', accessor: 'status'},
       // { Header: 'Distance to', accessor: 'distance'}
    ];

    const credentials = (id: number) => {
        return {
            username: 'cust' + id,
            password: 'cust' + id
        }
    }

    function parseResponse(input: Order[]) : TaxiOrder[] {
        let orderArr: TaxiOrder[] = [];
        if (!input || input.length === 0) {
            return [];
        }
        let order = input[0]; // we can show more in the future, not just current trip
        let rowArr: TripLeg[] = [];
        let inFound = false;
        let distance = 0;
        if (order.route && order.route.Status !== 'COMPLETED') { 
            for (let leg of order.route.Legs) {
                // find the first leg for this customer
                if (!inFound && leg.From === order.From) {
                    inFound = true;
                    rowArr.push(new TripLeg(
                        stopName(leg.From),
                        explain(leg.Status) === '' ? 'WAITING' : explain(leg.Status),
                        0 // TODO: count ETA
                    ));
                    distance = leg.Distance;
                } else if (inFound) {
                    rowArr.push(new TripLeg(
                        stopName(leg.From),
                        explain(leg.Status),
                        distance === 0 ? 1 : distance
                    ));
                    distance = leg.Distance; // we need to show the distance of the previous leg
                }
                if (inFound && leg.To == order.To) {
                    rowArr.push(new TripLeg(
                        stopName(leg.To),
                        explain_last(leg.Status),
                        leg.Distance === 0 ? 1 : leg.Distance
                    ));
                    break;
                }
            }
        }
        orderArr.push(new TaxiOrder(
            new TripOrder(
                order.Id, 
                order.Status,
                order.From,
                order.To,
                order.Distance,
                order.Wait,
                order.Loss,
                order.Shared,
                trimDate(order.Received),
                trimDate(order.Started),
                order.Completed ? order.Completed.toString() : "",
                order.AtTime,
                order.Eta,
                order.InPool,
                order.Cab,
                order.leg,
                order.route
            ), 
            rowArr
        ));
        return orderArr;
    }

    function trimDate(date: Date) : string {
        return date && date.toString().length > 2
            ? date.toString().replaceAll('T',' ').substring(0, 19) : "";
    }

    function explain(status: string) {
        switch(status) {
            case 'ASSIGNED': return '';
            case 'STARTED' : return 'LEFT BEHIND'
            case 'COMPLETED': return 'VISITED';
        }
        return '';
    }

    function explain_last(status: string) {
        switch(status) {
            case 'ASSIGNED': return '';
            case 'STARTED' : return 'APPROACHING'
            case 'COMPLETED': return 'VISITED';
        }
        return '';
    }

    function stopName(id: number) : string {
        let s = stops.find(i => i.id == id); // === would fail
        if (s) {
            return s.name;
        } 
        return "<unknown>";
    }

    function updateOrder(order_id: number, usr_id: number, status: String, ordrs: Array<TaxiOrder>) {
        axios.put(host + '/orders' /*+ button.name*/, 
                { Id: order_id, 
                Status : status,
                From: 0, // will be ignored
                To: 0,
                Wait: 0,
                Loss: 0
                }, 
                { auth: credentials(usr_id),
                headers: { 'Content-Type': 'application/json'} 
                })
            .then(function (response) {
                ordrs[0].order.status = 'ACCEPTED';
                setOrders(ordrs);
            })
            .catch(function (error) {
                alert(error);
            });
    }

    function getRoute() {
        if (stops.length === 0) { // still waiting for stops
            return; 
        }
        let custId = Number(window.localStorage.getItem('custid'));
        if (!custId) {
            custId = 0;
        }
        axios.get(host + '/orders', { auth: credentials(custId) })
        .then(response => {
            if (response.data && response.data.length > 0) {
                let ords: Order[] = response.data;
                if (ords[0].RouteId && ords[0].RouteId > -1) {
                    axios.get(host + '/routes/' + ords[0].RouteId, { auth: credentials(custId) })
                    .then(resp => {
                        if (resp.data) {
                            ords[0].route = resp.data/*
                            for (let leg of ords[0].route.Legs) {
                                if (leg.Id == ords[0].LegId) {
                                    ords[0].leg = leg;
                                    break;
                                }
                            }
                                */
                            if ((ords[0].Status === 'ASSIGNED' || ords[0].Status === 'ACCEPTED') &&
                                ords[0].From == ords[0].Cab?.Location) {
                                    ords[0].Status = 'PICKEDUP'; // TODO: what if PUT fails
                                    updateOrder(ords[0].Id, custId, 'PICKEDUP', parseResponse(ords));
                            } else if (ords[0].Status === 'PICKEDUP' && ords[0].To == ords[0].Cab?.Location) {
                                    ords[0].Status = 'COMPLETED';
                                    updateOrder(ords[0].Id, custId, 'COMPLETED', parseResponse(ords));
                            } else {
                                setOrders(parseResponse(ords));
                            }
                        }
                    });
                } else {
                    setOrders(parseResponse(ords));
                }
            }
        })
        .catch(function (error) {
            console.log(error);
        });;
    }

    useEffect(() => {
        getRoute();
        let interval = setInterval(() => getRoute(), 2000);
        return () => { clearInterval(interval); }
    }, [stops]);

    useEffect(() => {
        axios.get(host + '/stops', { auth: credentials(0) })
        .then(response => { 
            setStops(response.data)
        }); 
    }, []);


    function renderHeader(order: TripOrder) {
      return ( 
        <table style={{ padding: '0px' }} >
          <tbody>
            <tr><th align='right'>Cust ID : </th> <td align='left'>{window.localStorage.getItem('custid')}</td></tr>
            <tr><th align='right'>Order ID : </th> <td align='left'>{order.id}</td></tr>
            <tr><th align='right'>Status : </th>   <td align='left'>{order.status !== 'REFUSED' ? order.status : ""}</td></tr>
            <tr><th align='right'>From : </th>     <td align='left'>{stopName(order.fromStand)} ({order.fromStand})</td></tr>
            <tr><th align='right'>To : </th>       <td align='left'>{stopName(order.toStand)} ({order.toStand})</td></tr>
            <tr><th align='right'>Distance : </th> <td align='left'>{order.distance}</td></tr>
            <tr><th align='right'>Max wait : </th> <td align='left'>{order.maxWait}</td></tr>
            <tr><th align='right'>Shared : </th>   <td align='left'>{order.shared?"YES":"NO"}</td></tr>
            <tr><th align='right'>In pool : </th>  <td align='left'>{order.inPool?"YES":"NO"}</td></tr>
            <tr><th align='right'>Received : </th> <td align='left'>{order.received}</td></tr>
            <tr><th align='right'>Started : </th>  <td align='left'>{order.started}</td></tr>
            {  order.atTime ?
                <tr><th align='right'>At time : </th>  <td align='left'>{order.atTime.toString()}</td></tr>
                : <></>
            }
            <tr><th align='right'>ETA : </th>      <td align='left'>{order.eta}</td></tr>
            <tr><th align='right'>Cab : </th>      <td align='left'>{order.cab !== "-1" ? order.cab : "<unassigned>"}</td></tr>
            <tr><th align='right'>Route : </th>    <td align='left'>{order.route ? order.route.Id : ""}</td></tr>
          </tbody>
        </table>
      );
      /*
                  <tr><th align='right'>Dist. with pool : </th>
                <td align='left'>{countDistance(order)}</td></tr>
      */
    }

    function countDistance(order: TripOrder) : number {
        let foundStart = false;
        let sum = 0;
        let from = order.fromStand;
        let to = order.toStand;
        if (!order.route || !order.route.Legs) {
            return -1; // serious error, no legs
        }
        for (const l of order.route.Legs) {
            if (from === l.From) {
                foundStart = true;
                sum += (l.Distance === 0 ? 1 : l.Distance) + 1; // one minute (km) on the stop
            } 
            if (foundStart && from !== l.From) { // consecutive leg 
                sum += (l.Distance === 0 ? 1 : l.Distance) + 1;
            }
            if (to === l.To) { // it might be the same leg as above
                return sum - 1; //the time spent on the last stop does not count
            }
        }
        return -2; // some serious error - noe last leg
    }

    const buttonHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        //const button: HTMLButtonElement = event.currentTarget;
        updateOrder(orders[0].order.id, Number(window.localStorage.getItem('custid')), 'ACCEPTED', orders);
    };
      
    function renderRoutes() {
        return orders.map((item, index) => {
           return ( 
            <>
                {renderHeader(item.order)}
                { item.order.status === 'ASSIGNED' 
                  ? <button onClick={buttonHandler} name={item.order.id.toString()}>
                        Accept assignement
                    </button>
                  : <></>
                }
                { item && item.legs && item.legs.length > 0
                  ? <Table columns={columns} data={item.legs} />
                  : <></>
                }
            </>
           );
       });
    }

    const handleChange = (event: React.ChangeEvent<any>) : void => {
        const name = event.currentTarget.name;
        const value = event.currentTarget.type === 'checkbox' 
                    ? event.currentTarget.checked : event.currentTarget.value;
        setInputs(values => ({...values, [name]: value}))
      }
    
    function handleChange2(event: any) {
        if (event.target.value && event.target.value.length > 0) { 
            //setCustId(Number(event.target.value)); 
            window.localStorage.setItem('custid', event.target.value);
        }
    };
      
    function handleKeyDown(event: any) {
        if (event.key === 'Enter') {
            setOrders([]);
            getRoute();
            forceUpdate()
            setIsShown(false);
        }
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) : void => {
        event.preventDefault();
        let custId = Number(window.localStorage.getItem('custid'));
        if (!custId) {
            custId = 0;
        }
        axios.post(host + '/orders', 
            { id: 0, // ignored
                status: "RECEIVED", // ignored anyway
                From : Number(inputs.fromStand), // kapir does not like strings
                To : Number(inputs.toStand), 
                Wait : Number(inputs.maxWait), 
                Loss : Number(inputs.maxLoss), 
                Shared : inputs.shared,
                at_time : inputs.atTime ? inputs.atTime.replace('T',' ') + ":00" : inputs.atTime
            },
            { auth: credentials(custId),
                headers: { 'Content-Type': 'application/json' } 
            })
            .then(function (response) {
                setOrders(parseResponse(new Array(response.data)));
                //getRoute();
            })
            .catch(function (error) {
                alert(error);
            });
    }
    
    function justCompleted(dtStr: string) : boolean {
        if (dtStr) {
            const now = new Date().getTime();
            const offset = new Date(dtStr).getTimezoneOffset() * 60 * 1000;
            const dt = new Date(dtStr.replaceAll('T',' ').substring(0, 19)).getTime();
            if (Math.abs((offset + (dt - now))/(1000)) < 60) { // secs
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    return (
        <div className="wrapper">
            <div className="nav">
                <img src={logo} style={{ width: '40px' }} alt="logo" />
            </div>
            { orders && orders.length > 0 && (orders[0].order.status !== 'COMPLETED' || justCompleted(orders[0].order.completed))
              ? <div className="main">
                    <div onClick={() => setIsShown(true)}>
                        {isShown && (
                        <input name='id' autoFocus type="number" onChange={handleChange2} onKeyDown={handleKeyDown}/>)}
                        {!isShown && ("Customer: " + window.localStorage.getItem('custid'))}
                    </div>
                    {renderRoutes()}
                </div>
              : <form onSubmit={handleSubmit}>
                <h2 onClick={() => setIsShown(true)}> 
                    {isShown && (
                        <input name='id' autoFocus type="number" onChange={handleChange2} onKeyDown={handleKeyDown}/>)}
                    {!isShown && ("Request a bus: ")}
                </h2>
                <table><tbody>
                <tr>
                    <td align='right'><label>Cust ID :</label></td>
                    <td align='left'>
                        {window.localStorage.getItem('custid')}
                    </td>
                   </tr> 
                  <tr>
                    <td align='right'><label>From :</label></td>
                    <td align='left'>
                        <select name="fromStand" onChange={handleChange}>
                        {stops.map((stop, index) =>
                            <option key={index} value={stop.id}>
                               {stop.id}: {stop.name}
                            </option>
                        )}
                        </select>
                    </td>
                   </tr> 
                   <tr>
                    <td align='right'><label>To :</label></td>
                    <td align='left'>
                        <select name="toStand" onChange={handleChange}>
                        {stops.map((stop, index) =>
                            <option key={index} value={stop.id}>
                                {stop.id}: {stop.name}
                            </option>
                        )}
                        </select>
                    </td>
                   </tr> 
                   <tr>
                    <td align='right'><label>Max wait :</label></td>
                    <td align='left'>
                        <input 
                            type="number" 
                            name="maxWait"
                            value={inputs.maxWait || ""} 
                            onChange={handleChange}
                            style={{ width:'5ch' }}
                        />
                        <label> min</label>
                    </td>
                   </tr>
                   <tr>
                    <td align='right'><label>Shared? :</label></td>
                    <td align='left'>
                        <input 
                            type="checkbox" 
                            name="shared"
                            checked={inputs.shared} 
                            onChange={handleChange}
                        />
                    </td>
                   </tr>
                   <tr>
                    <td align='right'><label>Max loss if shared :</label></td>
                    <td align='left'>
                        <input 
                            type="number" 
                            name="maxLoss"
                            value={inputs.maxLoss || ""} 
                            onChange={handleChange}
                            style={{ width:'5ch' }}
                        />
                        <label> %</label>
                    </td>
                   </tr>
                   <tr>
                    <td align='right'><label>Required at :</label></td>
                    <td align='left'>
                        <input 
                            type="datetime-local" 
                            name="atTime"
                            value={inputs.atTime || ""} 
                            onChange={handleChange}
                        />
                    </td>
                   </tr>
                   <tr><td></td>
                       <td style={{ padding : "20px" }}>
                           <input type="submit" value="Send request" />
                        </td>
                   </tr>
                   </tbody></table>
                </form>
            }
            <QRCode
                    size={256}
                    style={{ height: "auto", maxWidth: "10%", width: "10%" }}
                    value={window.localStorage.getItem('custid') || "0"}
                    viewBox={`0 0 256 256`}
            />
        </div>
    )
}


class TaxiOrder  {
    order: TripOrder;
    legs: TripLeg[];
    constructor(order: TripOrder, legs: TripLeg[]) {
        this.order = order;
        this.legs = legs;
    }
}

class TripLeg {
    stop: string;
    status: string;
    distance: number;
    
    constructor(
        stop: string,
        status: string,
        distance: number
    ) {
        this.stop = stop;
        this.status = status;
        this.distance = distance;
    }
}

class TripOrder {
    id: number;
    status: string;
    fromStand: number;
    toStand: number;
    distance: number;
    maxWait: number;
    maxLoss: number;
    shared: boolean;
    received: string;
    started: string;
    completed: string;
    atTime?: Date;
    eta?: number;
    inPool: boolean;
    cab?: string;
    leg?: Leg;
    route?: Route;

    constructor(
        id: number,
        status: string,
        fromStand: number,
        toStand: number,
        distance: number,
        maxWait: number,
        maxLoss: number,
        shared: boolean,
        received: string,
        started: string,
        completed: string,
        atTime: Date,
        eta: number,
        inPool: boolean,
        cab?: Cab,
        leg?: Leg,
        route?: Route) {
            this.id = id;
            this.status = status;
            this.fromStand = fromStand;
            this.toStand = toStand;
            this.maxWait = maxWait;
            this.maxLoss = maxLoss;
            this.shared = shared;
            this.received = received;
            this.atTime = atTime;
            this.eta = eta > -1 ? eta : undefined;
            this.inPool = inPool;
            let cabId = cab && cab.Id ? cab.Id.toString() : "Waiting for assignment ...";
            this.cab = cab && cab.Name ? cab.Name : cabId;
            this.leg = leg;
            this.route = route;
            this.distance = distance;
            this.started = started;
            this.completed = completed;
    }
}
export interface Order {
    Id: number;
    Status: string;
    From: number;
    To: number;
    Distance: number
    Wait: number;
    Loss: number;
    Shared: boolean;
    Received: Date;
    Started: Date;
    Completed: Date;
    AtTime?: any;
    Eta: number;
    InPool: boolean;
    Cab?: Cab;
    Customer: Customer;
    LegId?: number;
    RouteId?: number;
    leg: Leg;
    route: Route;
}

export interface Customer {
    id: number;    
}

export interface Route {
    Id: number;
    Status: string;
    Cab: Cab;
    Legs: Leg[];
}

export interface Leg {
    Id: number;
    From: number;
    To: number;
    Distance: number;
    Place: number;
    Status: string;
    Started: Date;
    Completed: Date;
}

export interface Cab {
    Id: number;
    Location: number;
    Name: string;
    Status: string;
}

export interface Stop {
    id: number;
    no: string;
    name: string;
    type: string;
    bearing?: any;
    latitude: number;
    longitude: number;
}

export interface Input {
    status : string,
    fromStand : number,
    toStand : number,
    maxWait : number,
    maxLoss : number,
    shared : boolean,
    atTime : string
}